# Smart-Home mit Home-Assistant

## Installation auf einem Raspberry PI

[Offizielle Anleitung](https://www.home-assistant.io/installation/raspberrypi#install-home-assistant-operating-system)

## Erste Einrichtung

[Offizielle Anleitung](https://www.home-assistant.io/getting-started/onboarding/)

## Add-ons für Home-Assistant

### ESPHome

- [GitHub](https://github.com/esphome/esphome)
- [Getting Started with ESPHome and Home Assistant](https://esphome.io/guides/getting_started_hassio.html)
- [ESP Components](https://esphome.io/components/index.html?highlight=component)
- Initiales Flashen der Geräte: [ESPHome Web](https://web.esphome.io/?dashboard_wizard) *(ESPHome Web requires a browser that supports WebSerial. Please open this website on your desktop using Google Chrome or Microsoft Edge)*

### Node-Red

- [GitHub](https://github.com/hassio-addons/addon-node-red)
- [Getting Started](https://nodered.org/docs/getting-started/)

![node-red-config](img/node-red-config.png)
